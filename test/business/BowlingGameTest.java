package business;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BowlingGameTest {

	private BowlingGame game;
	
	@Before
	public void init() {
		game = new BowlingGame();
	}
	
	@Test
	public void should_throw_ball_without_scoring() {
		game.throwBall(0);
	}
	
	@Test
	public void should_score_zero_if_only_gutter_game() {
		game.roll(
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0);
		Assert.assertEquals(0, game.getScore());
	}
	
	@Test
	public void should_be_20_score_if_only_1_pin_knocked_game() {
		game.roll(
				1,1, 1,1,
				1,1, 1,1,
				1,1, 1,1,
				1,1, 1,1,
				1,1, 1,1);
		Assert.assertEquals(20, game.getScore());
	}
	
	@Test
	public void should_only_score_spare_followed_by_next_number_of_pins_game() {
		game.roll(
				7,3, 5,0,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0);
		Assert.assertEquals(20, game.getScore());
	}
	
	@Test
	public void should_only_score_strike_followed_by_next_2_numbers_of_pins_game() {
		game.roll(
				10, 3,5,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0,
				0,0, 0,0);
		Assert.assertEquals(26, game.getScore());
	}
	
	@Test
	public void should_score_perfect_game() {
		game.roll(
				10, 10,
				10, 10,
				10, 10,
				10, 10,
				10, 10,10,10);
		Assert.assertEquals(300, game.getScore());
	}
}
