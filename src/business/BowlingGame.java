package business;

public class BowlingGame {
	
	private int gameThrow = 0;
	private int[] gameThrows = new int[21];	// there can be 3 throws in last frame 
	

	public BowlingGame() {
		
	}
	
	public void throwBall(int pinsKnocked) {
		gameThrows[gameThrow++] = pinsKnocked;
	}

	public int getScore() {
		int score = 0;
		int cursor = 0;
		
		for (int frame = 0; frame < 10; frame++) {
			if (isStrike(cursor)) {
				score += 10 + gameThrows[cursor + 1] + gameThrows[cursor + 2];
				cursor++;
			}
			else if (isSpare(cursor)) {
				score += 10 + gameThrows[cursor + 2];
				cursor += 2;
			} else {
				score += gameThrows[cursor] + gameThrows[cursor + 1];			
				cursor += 2;
			}
		}
		
		return score;
	}

	private boolean isSpare(int cursor) {
		return gameThrows[cursor] + gameThrows[cursor + 1] == 10;
	}

	private boolean isStrike(int cursor) {
		return gameThrows[cursor] == 10;
	}
	
	public void roll(int... gameThrows) {
		for (int pinsKnocked : gameThrows) {
			throwBall(pinsKnocked);
		}
	}
}
